using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    //Todas las variables del jugador
    public float gravedad;
    public Vector2 velocidad;
    public float velocidadSalto = 20;
    public float aceleracion = 5;
    public float maximaAceleracion = 10;
    public float maximaVelocidadX = 50;
    public float alturaPiso = 10;
    public bool enPiso = false;
    public bool manteniendoSalto = false;
    public float maximoManteniendoSalto = 0.4f;
    public float timerMantenimientoSalto = 0.0f;
    public float topeManteniendoSalto = 0.4f;
    public float saltoPisoThreshold = 1;
    public float distancia = 0;
    public bool estaMuerto = false;

    //Layers para detectar y separar los pisos de los obstaculos
    public LayerMask pisoLayer;
    public LayerMask obstaculoLayer;

    private Animator anim;

    Controller_PisoCaida pisoCaida;
    Vector2 pos;

    private void Awake()
    {
        //Llamada al animator
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        Vector2 pos = transform.position;
        float distanciaPiso = Mathf.Abs(pos.y - alturaPiso);

        if(enPiso || distanciaPiso <= saltoPisoThreshold)
        {
            //Detecta el Input del jugador para saltar con el Espacio
            //Luego hace que enPiso sea falso, aplica la velocidad en Y para poder elevarse
            //Y tambien se pone en true el bool para activar la animacion de salto
            if (Input.GetKeyDown(KeyCode.Space))
            {
                enPiso = false;
                velocidad.y = velocidadSalto;
                manteniendoSalto = true;
                timerMantenimientoSalto = 0;
                anim.SetBool("Jump", true);

                if(pisoCaida != null)
                {
                    pisoCaida.jugador = null;
                    pisoCaida = null;
                }
            }
        }
        //Una vez que se suelta el espacio se pone en falso la variable y el bool de la animacion de salto
        if (Input.GetKeyUp(KeyCode.Space))
        {
            manteniendoSalto = false;
            anim.SetBool("Jump", false);
        }
    }

    private void FixedUpdate()
    {
        pos = transform.position;

        //Si esta muerto no continua
        if (estaMuerto)
        {
            return;
        }
        //Detecta que si el jugador esta en y -5 que seria lo mismo que caerse del piso pierde
        if(pos.y < -5)
        {
            estaMuerto = true;
        }
        //Llamada de Funciones
        detectorNoPiso();

        detectarPiso();

        creacionObstaculosPowerUp();

        transform.position = pos;
    }


    //Si se colisiona con el obstaculo se baja la velocidad
    void obstaculoGolpeado(Controller_Obstaculos obstaculos)
    {
        Destroy(obstaculos.gameObject);
        velocidad.x *= 0.7f;
    }

    //Si se colisiona con el powerUp se sube la velocidad
    void PowerUpGolpeado(Controller_PowerUp powerUp)
    {
        Destroy(powerUp.gameObject);
        velocidad.x *= 1.2f;
    }

    //Encargado de todo lo que pasa cuando el jugador esta en el aire
    void detectorNoPiso()
    {
        if (!enPiso)
        {
            //Al mantener el salto se empieza el timer
            //Si se llega a pasar del maximo se para el salto
            if (manteniendoSalto)
            {
                timerMantenimientoSalto += Time.fixedDeltaTime;
                if (timerMantenimientoSalto >= maximoManteniendoSalto)
                {
                    manteniendoSalto = false;
                }
            }

            pos.y += velocidad.y * Time.fixedDeltaTime;
            if (!manteniendoSalto)
            {
                velocidad.y += gravedad * Time.fixedDeltaTime;
            }

            //Se calcula con Raycast hacia abajo si el jugador cuando caiga va a pisar el piso o no
            //Esto para poder decidir si cae al vacio o no al estar en piso
            Vector2 origenRay = new Vector2(pos.x + 0.7f, pos.y);
            Vector2 direcRay = Vector2.up;
            float distanciaRay = velocidad.y * Time.fixedDeltaTime;
            //Aca se ponen varias variables como el donde sale el raycast, la direccion y la distancia, pero mas importante el Layer
            //Porque no es lo mismo caer en un piso que un obstaculo o Power Up
            RaycastHit2D hit2D = Physics2D.Raycast(origenRay, direcRay, distanciaRay, pisoLayer);
            //Detecta las colisiones
            if (hit2D.collider != null)
            {
                //Agarra el Collider del Piso
                Controller_Piso piso = hit2D.collider.GetComponent<Controller_Piso>();
                if (piso != null)
                {
                    //Aca se calcula si la posicion del jugador en Y es mayor o igual a la del piso es porque esta bien y sigue en piso
                    if (pos.y >= piso.alturaPiso)
                    {
                        alturaPiso = piso.alturaPiso;
                        pos.y = alturaPiso;
                        velocidad.y = 0;
                        enPiso = true;
                    }

                    //Se llama al controlador de la caida de los pisos
                    pisoCaida = piso.GetComponent<Controller_PisoCaida>();
                    if (pisoCaida != null)
                    {
                        //Hace que el jugador tambien caiga con el mismo, en Y
                        pisoCaida.jugador = this;
                    }
                }
            }
            Debug.DrawRay(origenRay, direcRay * distanciaRay, Color.red);

            //Similar al anterior, pero este detecta en X para ver si choca con alguna pared de los pisos
            Vector2 origenPared = new Vector2(pos.x, pos.y);
            RaycastHit2D paredHit = Physics2D.Raycast(origenPared, Vector2.right, velocidad.x * Time.fixedDeltaTime, pisoLayer);
            if (paredHit.collider != null)
            {
                Controller_Piso piso = paredHit.collider.GetComponent<Controller_Piso>();
                if (piso != null)
                {
                    if (pos.y < piso.alturaPiso)
                    {
                        //Al chocar con la pared, su velocidad baja en seco a 0
                        velocidad.x = 0;
                    }
                }
            }
        }
        distancia += velocidad.x * Time.fixedDeltaTime;
    }

    //Encargado de todo lo que pasa cuando el jugador esta en el piso
    void detectarPiso()
    {
        if (enPiso)
        {
            //Se encarga de que mientras mas tiempo pasa el jugador va aumentando la velocidad
            //Tiene un tope de vlocidad marcada por la variable Maxima Aceleracion
            float ratioVelocidad = velocidad.x / maximaVelocidadX;
            aceleracion = maximaAceleracion * (1 - ratioVelocidad);
            maximoManteniendoSalto = topeManteniendoSalto * ratioVelocidad;

            velocidad.x += aceleracion * Time.fixedDeltaTime;
            //Aca se marca el tope, para que no se rompa el juego
            if (velocidad.x >= maximaVelocidadX)
            {
                velocidad.x = maximaVelocidadX;
            }
            //Esto es similar a la funcion anterior solamente que detecta para la caida del jugador en el piso, y la velocidad en la que cae el mismo
            Vector2 origenRay = new Vector2(pos.x - 0.7f, pos.y);
            Vector2 direcRay = Vector2.up;
            float distanciaRay = velocidad.y * Time.fixedDeltaTime;
            if (pisoCaida != null)
            {
                distanciaRay = -pisoCaida.velocidadCaida * Time.fixedDeltaTime;
            }
            RaycastHit2D hit2D = Physics2D.Raycast(origenRay, direcRay, distanciaRay);
            if (hit2D.collider == null)
            {
                enPiso = false;
            }
            Debug.DrawRay(origenRay, direcRay * distanciaRay, Color.yellow);
        }
    }

    //Encargados en la deteccion de Obstaculos y PowerUps
    void creacionObstaculosPowerUp()
    {
        //Primero se crea una variable que va a contener la posicion del los obstaculos y los power ups
        //Esto para luego despues utilizarlo para detectar en el raycast si lo que choca es eso mismo
        Vector2 origenObstaculo = new Vector2(pos.x, pos.y);
        RaycastHit2D obstaculoColX = Physics2D.Raycast(origenObstaculo, Vector2.right, velocidad.x * Time.fixedDeltaTime, obstaculoLayer);
        if (obstaculoColX.collider != null)
        {
            //En los dos casos similares donde solo cambia la llamada a un objeto u otro
            //Se llama al colidder y si detecta que si es ese objeto en cuestion que choca se hace la funcion
            Controller_Obstaculos obstaculos = obstaculoColX.collider.GetComponent<Controller_Obstaculos>();
            if (obstaculos != null)
            {
                obstaculoGolpeado(obstaculos);
            }

            Controller_PowerUp powerUp = obstaculoColX.collider.GetComponent<Controller_PowerUp>();
            if (powerUp != null)
            {
                PowerUpGolpeado(powerUp);
            }
        }

        //Este es similar solamente que en vez de ser en X como el anterior es en Y
        //Por si el jugador llega a saltar y caer en el obstaculo o PowerUp
        RaycastHit2D obstaculoColY = Physics2D.Raycast(origenObstaculo, Vector2.up, velocidad.y * Time.fixedDeltaTime, obstaculoLayer);
        if (obstaculoColY.collider != null)
        {
            Controller_Obstaculos obstaculos = obstaculoColY.collider.GetComponent<Controller_Obstaculos>();
            if (obstaculos != null)
            {
                obstaculoGolpeado(obstaculos);
            }

            Controller_PowerUp powerUp = obstaculoColY.collider.GetComponent<Controller_PowerUp>();
            if (powerUp != null)
            {
                PowerUpGolpeado(powerUp);
            }
        }
    }
}
