using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Piso : MonoBehaviour
{
    Controller_Player jugador;

    public float alturaPiso;
    public float pisoDerecha;
    public float pantallaDerecha;
    BoxCollider2D collider;
    bool pisoGenerado = false;

    public Controller_Obstaculos obstaculos;
    public Controller_PowerUp powerUp;
    private void Awake()
    {
        jugador = GameObject.Find("Jugador").GetComponent<Controller_Player>();
        collider = GetComponent<BoxCollider2D>();
        alturaPiso = transform.position.y + (collider.size.y / 2);
        pantallaDerecha = Camera.main.transform.position.x * 2;
    }

    private void Update()
    {
        //Se actualiza constantemente la altura ya que al caer los pisos se necesita checkear constantemente esto
        alturaPiso = transform.position.y + (collider.size.y / 2);
    }

    private void FixedUpdate()
    {
        loopPisos();
    }

    //Funcion tronco del juego
    void generarPiso()
    {
        GameObject siguiente = Instantiate(gameObject);
        BoxCollider2D Collider = siguiente.GetComponent<BoxCollider2D>();
        Vector2 pos;

        //Se calcular 3 cosas, la altura 1 que se compone de la velocidad del salto multiplcado por el maximo al mantener
        //La altura 2 se calcula el tiempo que tardaria el jugador a saltar.
        //Y la 3 es otra vez la velocidad del salto multiplicado por el tiempo, gravedad, etc. 
        //Todo para poder calcular y sacar un random de la altura maxima y minima que puede crearse los pisos
        float h1 = jugador.velocidadSalto * jugador.maximoManteniendoSalto;
        float tiempo = jugador.velocidadSalto / -jugador.gravedad;
        float h2 = jugador.velocidadSalto * tiempo + (0.5f * (jugador.gravedad * (tiempo * tiempo)));
        float maximaAlturaSalto = h1 + h2;
        float maxY = maximaAlturaSalto * 0.4f;
        maxY += alturaPiso;
        float minY = -2;
        float actualY = Random.Range(minY, maxY);

        pos.y = actualY - Collider.size.y / 2;
        //Tope para que no se pase de la altura
        if(pos.y > -2f)
        {
            pos.y = 2f;
        }

        //Similar al calculo anterior solo que este se encarga solamente de la distancia en X
        //Osea que cada distancia de piso a piso es diferente, para darle dinamismo
        float t1 = tiempo + jugador.maximoManteniendoSalto;
        float t2 = Mathf.Sqrt((2.0f * (maxY - actualY)) / -jugador.gravedad);
        float tiempoTotal = t1 + t2;
        float maxX = tiempoTotal * jugador.velocidad.x;
        maxX *= 0.7f;
        maxX += pantallaDerecha;
        float minX = pantallaDerecha + 5;
        float actualX = Random.Range(minX, maxX);

        pos.x = actualX + Collider.size.x / 2;
        siguiente.transform.position = pos;

        Controller_Piso piso = siguiente.GetComponent<Controller_Piso>();
        piso.alturaPiso = siguiente.transform.position.y + (Collider.size.y / 2);

        //Detecta si se cayo el piso para destruirlo y hace calculo en random para ver cual edificio se cae o no
        Controller_PisoCaida caida = siguiente.GetComponent<Controller_PisoCaida>();
        if (caida != null)
        {
            Destroy(caida);
            caida = null;
        }
        if (Random.Range(0,5) == 0)
        {
            caida = siguiente.AddComponent<Controller_PisoCaida>();
            caida.velocidadCaida = Random.Range(0.3f, 1.0f);
        }

        //Calculo para crear los Obstaculos
        //Se crea un random que es la cantidad que salen por piso
        int numeroObstaculo = Random.Range(0, 4);
        for(int i = 0; i<numeroObstaculo; i++)
        {
            //Se instancia el obstaculo
            GameObject caja = Instantiate(obstaculos.gameObject);
            //Luego se calcula la posicion random del obstaculo
            //La cual se compone por la altura actual del piso
            //La mitad del objeto, esto para que no quede colgando el obstculo por los costado
            //Y Luego se calcula la nueva posicion del obstaculo
            float y = piso.alturaPiso;
            float mitadAltura = Collider.size.x / 2 - 1;
            float izquierda = siguiente.transform.position.x - mitadAltura;
            float derecha = siguiente.transform.position.x + mitadAltura;
            float x = Random.Range(izquierda, derecha);
            Vector2 posicionCaja = new Vector2(x, y);
            caja.transform.position = posicionCaja;

            //Y luego se agrega el objeto a la lista correspondiente
            if(caida != null)
            {
                Controller_Obstaculos o = caja.GetComponent<Controller_Obstaculos>();
                caida.obstaculos.Add(o);
            }
        }

        //Hace lo mismo que el de obstaculos solo que cambia a PowerUps, la lista correspondiente y la cantidad de objetos que aparecen
        int numeroPowerUp = Random.Range(0, 2);
        for (int i = 0; i < numeroPowerUp; i++)
        {
            GameObject powerUpCaja = Instantiate(powerUp.gameObject);
            float y = piso.alturaPiso;
            float mitadAltura = Collider.size.x / 2 - 1;
            float izquierda = siguiente.transform.position.x - mitadAltura;
            float derecha = siguiente.transform.position.x + mitadAltura;
            float x = Random.Range(izquierda, derecha);
            Vector2 posicionCaja = new Vector2(x, y);
            powerUpCaja.transform.position = posicionCaja;

            if (caida != null)
            {
                Controller_PowerUp o = powerUpCaja.GetComponent<Controller_PowerUp>();
                caida.powerUp.Add(o);
            }
        }
    }


    //Recibe posicion del jugador, y la posicion de donde viene el piso a la derecha, mientras se mueve se detecta si no se ve en pantalla y si es asi se destruye
    //Si el booleano piso generado es falso se pregunta si la posicion del piso a la derecha es menor a la parte derecha de la pantalla
    //Si esto cumple se crea el piso con la funcion Generar Piso
    void loopPisos()
    {
        if (!jugador.estaMuerto)
        {
            Vector2 pos = transform.position;
            pos.x -= jugador.velocidad.x * Time.fixedDeltaTime;

            pisoDerecha = transform.position.x + (collider.size.x / 2);

            if (pisoDerecha < -10)
            {
                Destroy(gameObject);
                return;
            }

            if (!pisoGenerado)
            {
                if (pisoDerecha < pantallaDerecha)
                {
                    pisoGenerado = true;
                    generarPiso();
                }
            }

            transform.position = pos;
        }
    }
}
