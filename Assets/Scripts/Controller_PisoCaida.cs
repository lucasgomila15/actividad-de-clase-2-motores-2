using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PisoCaida : MonoBehaviour
{

    bool debeCaer = false;
    public float velocidadCaida = 1;

    //Se llama al Jugador y se crean unas listas de Obstaculos y Power Up para mejor manera de controlarlos en los Pisos
    public Controller_Player jugador;
    public List<Controller_Obstaculos> obstaculos = new List<Controller_Obstaculos>();
    public List<Controller_PowerUp> powerUp = new List<Controller_PowerUp>();

    private void Awake()
    {
        jugador = GameObject.Find("Jugador").GetComponent<Controller_Player>();
    }

    private void FixedUpdate()
    {
        controladorPisoCaida();
    }


    //Como el nombre dice, se detecta la altura del piso del Jugador y la posicion actual del mismo para calcular la caida del piso
    //Tambien se hace el calculo de cada Obstaculo y PowerUp en la lista, esta info de manda al piso Controller
    void controladorPisoCaida()
    {
        if (debeCaer)
        {
            Vector2 pos = transform.position;
            float cantidadCaida = velocidadCaida * Time.fixedDeltaTime;
            pos.y -= cantidadCaida;

            //Hace que se caiga tambien el jugador a la velocidad de la caida del piso
            if (jugador != null)
            {
                jugador.alturaPiso -= cantidadCaida;
                Vector2 posJugador = jugador.transform.position;
                posJugador.y -= cantidadCaida;
                jugador.transform.position = posJugador;
            }

            //Listas de objetos y powerUps, donde hacen que caigan a la velocidad de la caida del piso
            foreach (Controller_Obstaculos o in obstaculos)
            {
                if (o != null)
                {
                    Vector2 oPos = o.transform.position;
                    oPos.y -= cantidadCaida;
                    o.transform.position = oPos;
                }
            }
            foreach (Controller_PowerUp o in powerUp)
            {
                if (o != null)
                {
                    Vector2 oPos = o.transform.position;
                    oPos.y -= cantidadCaida;
                    o.transform.position = oPos;
                }
            }

            transform.position = pos;
        }
        else
        {
            if (jugador != null)
            {
                debeCaer = true;
            }
        }
    }
}
