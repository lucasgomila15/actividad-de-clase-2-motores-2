﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public float depth = 1;

    Controller_Player jugador;

    private void Awake()
    {
        jugador = GameObject.Find("Jugador").GetComponent<Controller_Player>();
    }

    private void FixedUpdate()
    {
        parallax();
    }

    //Funcion Encargada del Parallax
    void parallax()
    {
        //Si esta muerto se deja de mover
        if(!jugador.estaMuerto)
        {
            //Se agarra la velocidad del jugador y segun el Depth que le pongamos va a ir mas lento o mas rapido
            float velocidadReal = jugador.velocidad.x / depth;
            Vector2 pos = transform.position;

            pos.x -= velocidadReal * Time.fixedDeltaTime;

            //Cuando algun objeto del parallax llega a -30 en X se manda a X 20, para que vuelva y siga en un loop
            if (pos.x <= -30)
            {
                pos.x = 20;
            }

            transform.position = pos;
        }
    }
}
