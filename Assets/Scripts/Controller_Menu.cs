using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller_Menu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GestorDeAudio.instancia.ReproducirSonido("Cancion1");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void Jugar()
    {
        SceneManager.LoadScene("RunnerScene");
    }
}
