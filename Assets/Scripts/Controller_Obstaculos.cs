using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Obstaculos : MonoBehaviour
{
    Controller_Player jugador;

    private void Awake()
    {
        jugador = GameObject.Find("Jugador").GetComponent<Controller_Player>();
    }

    private void FixedUpdate()
    {
        distanciaAJugador();
    }


    //Detecta la distancia al Jugador,  si es que esta lejos se destruye
    void distanciaAJugador()
    {
        if (!jugador.estaMuerto)
        {
            Vector2 pos = transform.position;

            pos.x -= jugador.velocidad.x * Time.fixedDeltaTime;
            if (pos.x < -1000)
            {
                Destroy(gameObject);
            }

            transform.position = pos;
        }
    }
}
