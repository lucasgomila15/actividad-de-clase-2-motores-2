using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_PowerUp : MonoBehaviour
{
    Controller_Player jugador;

    private void Awake()
    {
        //Llamada al Script del Jugador
        jugador = GameObject.Find("Jugador").GetComponent<Controller_Player>();
    }

    private void FixedUpdate()
    {
        detectarPosicionJugador();
    }


    //Es el mismo codigo que el de Obstaculos, hace la misma funcion, pero por temas de no mezclar scripts para cosas distintas decidi separarlos
    void detectarPosicionJugador()
    {
        if (!jugador.estaMuerto)
        {
            Vector2 pos = transform.position;

            pos.x -= jugador.velocidad.x * Time.fixedDeltaTime;
            if (pos.x < -1000)
            {
                Destroy(gameObject);
            }

            transform.position = pos;
        }
    }
}
