﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using TMPro;

public class Controller_Hud : MonoBehaviour
{
    Controller_Player jugador;
    TMP_Text distanciaTexto;
    TMP_Text mensajeTexto;
    public GameObject resultados;
    public TMP_Text distanciaResultadoTexto;

    private void Awake()
    {
        //Llamada
        jugador = GameObject.Find("Jugador").GetComponent<Controller_Player>();
        distanciaTexto = GameObject.Find("TextoDistancia").GetComponent<TMP_Text>();
        mensajeTexto = GameObject.Find("Mensaje").GetComponent<TMP_Text>();
        resultados.SetActive(false);
    }


    private void Update()
    {
        //Para calcular la distancia y ponerla en el texto de Distancia
        int distance = Mathf.FloorToInt(jugador.distancia);
        distanciaTexto.text = distance + " m";

        //Y si esta muerto se prende los resultados y se borra el distancia para que apararezca el resultado en el otro hud
        if (jugador.estaMuerto)
        {
            distanciaTexto.text = " ";
            resultados.SetActive(true);
            distanciaResultadoTexto.text = distance + " m";
            mensajeTexto.text = " ";
        }

        mensajes();
    }

    void mensajes()
    {
        int distance = Mathf.FloorToInt(jugador.distancia);
        if (distance < 600)
        {
            mensajeTexto.text = " ";
        }
        if (distance >= 600)
        {
            mensajeTexto.text = "No te caigas!";
        }
        else if (distance > 1000)
        {
            mensajeTexto.text = "Sos God!";
        }
        else if (distance > 1500)
        {
            mensajeTexto.text = "Nadie te puede parar!";
        }
    }

    //Funciones de los resultados
    public void Salir()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Retry()
    {
        SceneManager.LoadScene("RunnerScene");
    }
}
